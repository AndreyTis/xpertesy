package PositiveTests;

import Consts.Consts;
import Pages.*;
import Utils.ShareDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LogInRegisterTests extends BasePage{

    private static WebDriver driver;
    private static MainPage mainPage;
    private static Dashboard dashboard;
    private static WebDriverWait wait;

// LogIn Positive Tests

    @BeforeAll
    public static void setupDriver() {
        mainPage = new MainPage();
        dashboard = new Dashboard();
        driver = ShareDriver.getWebDriver();
    }

    @BeforeEach
    public void beforeEachSetup() {
        driver.get(Consts.MAIN_URL);
        wait = new WebDriverWait(driver, 5);
    }

    @AfterAll
    public static void closeDriver() {
        ShareDriver.closeDriver();
        driver = null;
    }

    // Home Page Logo Test

    @Test
    public void HomePageTest() {

        WebElement mainPageLogoExists = mainPage.mainPage();
        assertNotNull(mainPageLogoExists);
    }

    // LogIn/LogOut Positive Test

    @Test
    public void logInLogOutTest() {

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);
        WebElement logInHeader = logInRegisterPage.logInHeaderExist();
        assertNotNull(logInHeader);

        WebElement emailLogInField = BasePage.sendTextToField(Consts.EMAIL_LOGIN_FIELD, Consts.EMAIL);
        assertNotNull(emailLogInField);

        WebElement passwordLogInField = BasePage.sendTextToField(Consts.PASSWORD_LOGIN_FIELD, Consts.PASSWORD);
        assertNotNull(passwordLogInField);

        WebElement element = logInRegisterPage.clickLogInButtonPositiveTest(Consts.LOG_IN_BUTTON);
        assertNotNull(element);

        WebElement dashboardHeader = dashboard.dashboardManePage();
        assertNotNull(dashboardHeader);
        dashboard.clickButton(Consts.USER_AVATAR_BUTTON);
        dashboard.clickButton(Consts.DASHBOARD_LOGOUT_BUTTON);
    }

    // Register Positive Test

    @Test
    public void registerTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);

        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.PASSWORD);
        WebElement emailAlreadyExistAlert = logInRegisterPage.clickRegisterButtonPositiveTest(Consts.REGISTER_BUTTON);
        assertNotNull(emailAlreadyExistAlert);
    }
}
