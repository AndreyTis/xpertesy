package NegativeTests;

import Consts.Consts;
import Pages.*;
import Utils.ShareDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LogInRegisterTests extends BasePage {

    private static WebDriver driver;
    private static MainPage mainPage;
    private static WebDriverWait wait;

    @BeforeAll
    public static void BeforeAllSetup() {
        mainPage = new MainPage();
        driver = ShareDriver.getWebDriver();
    }

    @BeforeEach
    public void beforeEachSetup() {
        driver.get(Consts.MAIN_URL);
        wait = new WebDriverWait(driver, 5);
    }

    @AfterAll
    public static void closeDriver() {
        ShareDriver.closeDriver();
        driver = null;
    }

    // Home Page Logo Test
    @Test
    public void HomePageTest() {

        WebElement mainPageLogoExists = mainPage.mainPage();
        assertNotNull(mainPageLogoExists);
    }

    // LogIn  Negative test with valid password and invalid email

    @ParameterizedTest
    @ValueSource(strings = {Consts.EMPTY_EMAIL, Consts.INCORRECT_EMAIL})
    public void negativeEmailTests(String text) {

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);
        WebElement logInHeader = logInRegisterPage.logInHeaderExist();
        assertNotNull(logInHeader);

        LogInRegisterPage.sendTextToField(Consts.EMAIL_LOGIN_FIELD, text);
        LogInRegisterPage.sendTextToField(Consts.PASSWORD_LOGIN_FIELD, Consts.PASSWORD);
        WebElement element = LogInRegisterPage.clickLogInButtonNegativeTest(Consts.LOG_IN_BUTTON);
        assertNotNull(element);
    }

    // LogIn  Negative test with valid email and invalid password

    @ParameterizedTest
    @ValueSource(strings = {Consts.EMPTY_PASSWORD, Consts.INCORRECT_PASSWORD})
    public void negativePasswordTests(String text) {

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);
        WebElement logInHeader = logInRegisterPage.logInHeaderExist();
        assertNotNull(logInHeader);

        LogInRegisterPage.sendTextToField(Consts.EMAIL_LOGIN_FIELD, Consts.EMAIL);
        LogInRegisterPage.sendTextToField(Consts.PASSWORD_LOGIN_FIELD, text);
        WebElement element = LogInRegisterPage.clickLogInButtonNegativeTest(Consts.LOG_IN_BUTTON);
        assertNotNull(element);
    }

//  Register Negative Tests

    @Test
    public void homeRegisterLogInButtonTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);
    }

    // empty first name field in register form

    @Test
    public void emptyFirstNameRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.EMPTY_FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }

    // empty last name field in register form

    @Test
    public void emptyLastNameRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.EMPTY_LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }

    // empty email field in register form

    @Test
    public void emptyEmailRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMPTY_EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }

    // incorrect email in register form

    @Test
    public void wrongEmailRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.WRONG_EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }

    // empty password field in register form

    @Test
    public void emptyPasswordRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.EMPTY_PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }

    // empty repeat password field in register form

    @Test
    public void emptyRepeatPasswordRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.EMPTY_REPEAT_PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }

    // incorrect repeat password field in register form

    @Test
    public void incorrectRepeatPasswordRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.INCORRECT_REPEAT_PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }

    // all empty  fields in register form

    @Test
    public void allEmptyFieldsRegisterTest() {

        mainPage.logInRegisterPage(Consts.LOG_IN_REGISTER_BUTTON);

        LogInRegisterPage logInRegisterPage = mainPage.logInRegisterPage(Consts.MAIN_PAGE_REGISTER_BUTTON);
        WebElement registerHeader1 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader1);

        mainPage.logInRegisterPage(Consts.I_AM_STUDENT_BUTTON);
        WebElement registerHeader2 = logInRegisterPage.registerHeaderExist();
        assertNotNull(registerHeader2);

        sendTextToField(Consts.FIRST_NAME_FIELD, Consts.EMPTY_FIRST_NAME);
        sendTextToField(Consts.LAST_NAME_FIELD, Consts.EMPTY_LAST_NAME);
        sendTextToField(Consts.EMAIL_REGISTER_FIELD, Consts.EMPTY_EMAIL);
        sendTextToField(Consts.PASSWORD_REGISTER_FIELD, Consts.EMPTY_PASSWORD);
        sendTextToField(Consts.REPEAT_PASSWORD_FIELD, Consts.EMPTY_REPEAT_PASSWORD);
        WebElement letsCreateYourAccountHeader = logInRegisterPage.clickRegisterButtonNegativeTest(Consts.REGISTER_BUTTON);
        assertNotNull(letsCreateYourAccountHeader);
    }
}
