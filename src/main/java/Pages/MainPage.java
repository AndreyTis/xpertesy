package Pages;

import Consts.Consts;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPage extends BasePage {

    protected static WebDriver driver;

    public WebElement mainPage() {
        WebElement mainPageLogo = findElementByXpath(Consts.MAIN_PAGE_LOGO);
        return mainPageLogo;
    }

    public LogInRegisterPage logInRegisterPage(String xpath) {
        clickElementByXpath(xpath);
        return new LogInRegisterPage();
    }
}
