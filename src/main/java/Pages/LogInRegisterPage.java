package Pages;

import Consts.Consts;
import org.openqa.selenium.WebElement;

public class LogInRegisterPage extends BasePage {

    public WebElement logInHeaderExist() {
        return findElementByXpath(Consts.LOG_IN_HEADER);
    }

    public WebElement registerHeaderExist() {
        return findElementByXpath(Consts.CREATE_ACCOUNT_HEADER);
    }

    public WebElement clickLogInButtonPositiveTest(String text) {
        clickElementByXpath(text);
        return findElementByXpath(Consts.DASH_BOARD_HEADLINE);
    }

    public static WebElement clickLogInButtonNegativeTest(String text) {
        clickElementByXpath(text);
        return findElementByXpath(Consts.LOG_IN_HEADER);
    }

    public WebElement clickRegisterButtonPositiveTest(String text) {
        clickElementByXpath(text);
        return findElementByXpath(Consts.EMAIL_ALREADY_EXIST_ALERT);
    }

    public WebElement clickRegisterButtonNegativeTest(String text) {
        clickElementByXpath(text);
        return findElementByXpath(Consts.CREATE_ACCOUNT_HEADER);
    }

    public WebElement incorrectRepeatPasswordNegativeTest(String text) {
        clickElementByXpath(text);
        return findElementByXpath(Consts.PASSWORD_DO_NOT_MATCH);
    }

    public WebElement wrongEmailFieldNegativeTest(String text) {
        clickElementByXpath(text);
        return findElementByXpath(Consts.WRONG_EMAIL);
    }
}