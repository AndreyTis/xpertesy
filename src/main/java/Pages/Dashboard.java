package Pages;


import Consts.Consts;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Dashboard extends BasePage{

    protected static WebDriver driver;

    public WebElement dashboardManePage() {
        WebElement dashboardHeadline = findElementByXpath(Consts.DASH_BOARD_HEADLINE);
        return dashboardHeadline;
    }

//    public  WebElement clickButton(String text) {
//        clickElementByXpath(text);
//        return findElementByXpath(Consts.DASH_BOARD_HEADLINE);
//    }

    public  void clickButton(String text) {
        clickElementByXpath(text);
    }
}
