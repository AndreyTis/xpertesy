package Pages;

import Utils.ShareDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;

public class BasePage {

    protected static WebDriver driver;

    protected static void clickElementByXpath(String xpath) {
        driver = ShareDriver.getWebDriver();
        findElementByXpath(xpath).click();
    }

    public static WebElement findElementByXpath(String xpath) {
        driver = ShareDriver.getWebDriver();
        WebElement element = driver.findElement(By.xpath(xpath));
        return element;
    }

    public static WebElement sendTextToField(String xpath, String text) {
        driver = ShareDriver.getWebDriver();
        WebElement element = findElementByXpath(xpath);
        element.sendKeys(text);
        return element;
    }
}