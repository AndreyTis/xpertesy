package Consts;

public class Consts {

    public static final String MAIN_PAGE_LOGO = "//img[@src='images/logo.svg']";
    public static final String MAIN_URL = "https://stg.xpertesy.com/index.php";
    public static final String LOG_IN_REGISTER_BUTTON = "//*[text()='Log In / Register']";
    public static final String LOG_IN_HEADER = "//div[@class='welcome-text']/h3";
    public static final String MAIN_PAGE_REGISTER_BUTTON = "//a[text()='Register']";
    public static final String CREATE_ACCOUNT_HEADER = "//div[@class='welcome-text registration-text']/h3";
    public static final String ORGANIZATION_ID_FIELD = "//input[@placeholder='Organization ID (Optional)']";
    public static final String I_AM_STUDENT_BUTTON = "//label[@for='student-radio']";
    public static final String FIRST_NAME_FIELD = "//input[@placeholder='First Name']";
    public static final String LAST_NAME_FIELD = "//input[@placeholder='Last Name']";
    public static final String EMAIL_REGISTER_FIELD = "//input[@id='emailaddress-register']";
    public static final String EMAIL_LOGIN_FIELD = "//input[@name='emailaddress-login']";
    public static final String PASSWORD_REGISTER_FIELD = "//input[@id='password-register']";
    public static final String PASSWORD_LOGIN_FIELD = "//input[@name='password-login']";
    public static final String REPEAT_PASSWORD_FIELD = "//input[@id='password-repeat-register']";
    public static final String REGISTER_BUTTON = "//button[@form='register-account-form']";
    public static final String EMAIL_ALREADY_EXIST_ALERT = "//div[@id='register-notification']";
    public static final String LOG_IN_BUTTON = "//button[@form='login-form']";
    public static final String DASH_BOARD_HEADLINE = "//div[@class='dashboard-headline']";
    public static final String FIRST_NAME = "Andrey";
    public static final String LAST_NAME = "T";
    public static final String USER_AVATAR_BUTTON = "//div[@class='user-avatar status-online']";
    public static final String DASHBOARD_LOGOUT_BUTTON = "//ul[@class='user-menu-small-nav']/li[4]/a";

    public static final String EMAIL = "tisandrey@yahoo.com";
    public static final String EMPTY_EMAIL = "";
    public static final String INCORRECT_EMAIL = "12345@gmail.com";
    public static final String WRONG_EMAIL = "12345";

    public static final String PASSWORD = "TisAndrey";
    public static final String EMPTY_PASSWORD = "";
    public static final String INCORRECT_PASSWORD = "12345";

    public static final String EMPTY_FIRST_NAME = "";
    public static final String EMPTY_LAST_NAME = "";
    public static final String EMPTY_REPEAT_PASSWORD = "";
    public static final String INCORRECT_REPEAT_PASSWORD = "12345";
    public static final String PASSWORD_DO_NOT_MATCH = "//div[@class='closeable error notification']";
    public static final String PROBLEM_SENDING_EMAIL = "//div[@class='closeable error notification']";




//      "//*[@class='user-menu-small-nav']/ul/li[4]/a";


}
